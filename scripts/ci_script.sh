#!/usr/bin/env sh

set -eu

(
  cd "$(dirname "$0")"/../

  scripts/build.sh
  scripts/build_doc.sh
  scripts/test.sh
)
