#!/usr/bin/env sh

set -eu

(
  cd "$(dirname "$0")"/../

  # shellcheck disable=SC2046
  eval $(opam config env)

  # Testing scripts
  shellcheck scripts/*.sh

  # Testing OCaml code
  oasis setup
  ocaml setup.ml -configure --enable-tests
  make test
  ./test.native
)
