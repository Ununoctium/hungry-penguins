# Hungry Penguins [![build status](https://gitlab.com/softwa/hungry-penguins/badges/master/build.svg)](https://gitlab.com/softwa/hungry-penguins/commits/master)

Hungry Penguins game written in [OCaml]. Done as part of the M1 Jacques Herbrand (MPRI) formation at ENS Paris-Saclay.

## Get started

### Dependencies

You need `ocaml`, `opam` and `m4` on your system. For Debian and derivatives see `scripts/ci_before_script.sh`. Then:

```sh
eval $(opam config env)
opam install -y oasis alcotest
```

### Build

```sh
scripts/build.sh
```

### Install

Coming soon.

### Try it

Coming soon.

## Contributing

See [CONTRIBUTING].

[OCaml]: https://ocaml.org/
[CONTRIBUTING]: https://gitlab.com/softwa/hungry-penguins/blob/master/CONTRIBUTING.md
