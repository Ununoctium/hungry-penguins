(** Client module *)

open Game_state

let log s = print_string s

let init_client () =

  (* TODO: parse this from args *)
  let port = 10234 in
  let machine = Unix.gethostname () in

  log ("trying to connect to server " ^ machine ^ " on port " ^ (string_of_int port) ^ "...\n");

  Network.connect_to_server machine port;

  log "connected, waiting for the game to start...\n"

let rec wait_msg () =
  try Network.get_server_msg ()
  with Network.No_Msg -> wait_msg ()

let _ =

  init_client ();

  let initialState = wait_msg () in

  let textView = Game_state.to_string initialState in

  print_string textView;

  Network.disconnect_from_server ();

  ()
