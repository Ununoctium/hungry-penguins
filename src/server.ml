(** Server module *)

open Game_state

let log s = print_string s

let init_server () =

  (* TODO: handle this in a better way *)
  let port = 10234 in
  let nbOfPlayers = 2 in

  log ("starting a server with port = " ^ (string_of_int port) ^ " ; nbj = " ^ (string_of_int nbOfPlayers) ^ "...\n");

  Network.server_start port;

  let clients = Array.init nbOfPlayers (fun n -> log ("waiting for client " ^ (string_of_int n)^ " to connect\n"); Network.wait_client_connection ()) in

  log "the game will start soon\n";

  clients

let send_game_state_to_clients gameState clients =
  Array.iter (fun client -> Network.send_msg_to_client gameState client) clients

let client_requests_to_string requests =
  List.map (fun req -> match req with
      | Network.ClientDisconnect(_) -> "logout"
      | Network.ClientMsg(_, msg) -> msg
    ) requests

let get_clients_msg clients =

  Array.map (fun client -> client_requests_to_string (Network.read_clients_requests [client])) clients

let treat_player_action gameState playerNum actions =
  List.iter (fun action ->
      if action = "logout" then ()
      else ()
    ) actions

let treat_players_actions gameState actions =
  Array.iteri (fun playerNum actions -> treat_player_action gameState playerNum actions) actions

let _ =

  let clients = init_server () in

  (* TODO: fix this *)
  let nbOfPlayers = (Array.length clients) in

  let rec mainloop gameState =
    if not (Game_state.game_over gameState) then begin
      send_game_state_to_clients gameState clients;
      let msg = get_clients_msg clients in
      treat_players_actions gameState msg;
      mainloop gameState
    end else begin
      send_game_state_to_clients gameState clients
    end
  in

  let initialGameState = Game_state.init nbOfPlayers in

  send_game_state_to_clients initialGameState clients;

  (* mainloop initialGameState; *)

  ()
