open Game_state
open Penguin
open Player
open Move
open Board

exception Pascuila
exception Pasparla

(**Function describing AI behaviour*)


(**Return the number of fish at position p.*)
let nb_fish (situation: game_state) (p: pos):int=
	let x = fst(p) and y = snd(p) and g = fst(situation) in
	g.(x).(y)

(**Return the case in the list with the most fish on it, and the number of fish on it.*)
let rec max_fish (l: pos list) (situation: game_state) (max:int) (ind_max:int) (actuel:int): (int * int)=
	(* à appeller avec max =0 et actuel = ind_max = 0*)
	(* actuel est le numéro de la case courante dans la liste totale, ind_max est l'indice maximum dans la liste*)
	match l with
	|[] -> (ind_max, max)
	|t::q -> let a = (nb_fish situation t) in
		if (a > max) then max_fish q situation a actuel (actuel+1)
			else max_fish q situation max ind_max (actuel+1)
	

(**Return distance of best case to go in the direction d for the penguin cuila, and the value of said case*)
let choose_distance situation cuila d fct_max=
	(* on peurt utiliser n'importe quel fonction de calcul de l'optimal, du moment qu'elle a le même type que max_fish*)
	let l = check_mvt (fst(situation)) (snd(situation)) (fst(cuila)) d in
	if (l=[]) then raise Pasparla;
	(fct_max l situation 0 (fst(cuila)) )





(**Select the best direction to go towards*)
let choose_direction situation cuila fct_max=
	let d = ref N and distance = ref -1 and max = ref -1 in
	let aux d =
		try 
			let a = fst(choose_distance situation cuila SW fct_max) in
			if (snd(a)> max) then (max:=a ; distance := fst(a); d:=SW)
		with
		| Pasparla-> ()	in
	List.iter aux all_directions
	if (!max = -1) then raise Pascuila;
	(!d, !distance, !max)




let determine_max (situation:game_state) (joueur:player): (penguin * dir* int)=
	let grille = fst(situation) and manchots = snd(situation) in
	let l = playerspenguins joueur (snd(situation)) in
	let aux x p =
		try
			let (a,b,c) = choose_direction situation p max_fish in
			(n,a,b,c)
		with
		| Pascuila -> (N,0,-1)
	in
	let l2 = map aux l in
	let aux2 = function (_,_,_,a) -> a
	in
	let aux3 x y = if ( (aux2 x) > (aux2 y) ) then x else y
	in
	let (a,b,c,d) = List.fold_left aux3 l2 in
	(l.(a),b,c)








