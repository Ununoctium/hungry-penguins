(** Board module *)

type fish = int
type grid = fish array array
type pos = int * int

exception InvalidGridSize

let empty_pos = 0, 0

let empty_grid h w = Array.make_matrix h w 0

let get_col_nb grid = Array.length grid

let get_line_nb grid =
  if Array.length grid > 0 then
    Array.length (grid.(0))
  else raise InvalidGridSize

let get_fish_nb (x, y) grid = grid.(x).(y)

let fish_to_string f = string_of_int f
