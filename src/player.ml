(** Player module *)

type player = Red | Blue | Green | Yellow

exception InvalidPlayerIndex

let empty = Red

let of_num = function
  | 0 -> Red
  | 1 -> Blue
  | 2 -> Green
  | 3 -> Yellow
  | _ -> raise InvalidPlayerIndex

let to_string = function
  | Red -> "R"
  | Blue -> "B"
  | Green -> "G"
  | Yellow -> "Y"
