(** Penguin module *)

type penguin = Board.pos * Player.player

let empty = Board.empty_pos, Player.empty
