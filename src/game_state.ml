(** Game_state module *)

type penguins = Penguin.penguin array
type game_state = Board.grid * penguins
type block_content = Fish of Board.fish | Player of Player.player

exception InvalidPlayerNumber
exception PlayerFound of Player.player

let playerspenguins (joueur:Player.player) (manchots: penguins)=
	let aux p= (snd(p)=joueur) in
	Array.fold_left (fun acc el -> if aux el then el::acc else acc) [] manchots

let penguins_per_player = function
  | 2 -> 4
  | 3 -> 3
  | 4 -> 2
  | _ -> raise InvalidPlayerNumber

let init nbOfPlayers =

  let penguinsPerPlayer = penguins_per_player nbOfPlayers in

  let nbOfPenguins = penguinsPerPlayer * nbOfPlayers in

  let penguins = Array.make nbOfPenguins Penguin.empty in

  for i = 0 to nbOfPlayers - 1 do
    for j = 0 to penguinsPerPlayer - 1 do
      Array.set penguins (penguinsPerPlayer * i + j) (Board.empty_pos, Player.of_num i)
    done;
  done;

  let grid = Board.empty_grid 4 15 in

  (* TODO: set correct number of fish on the grid *)
  (* TODO: each player choose where to put its penguins *)

  grid, penguins

(* TODO: fix this *)
let game_over gameState = false

let get_player_on pos penguins =
  try Array.iter (fun (pos', player) ->
      if pos = pos' then raise (PlayerFound player)
    ) penguins; None
  with PlayerFound p -> Some p

let get_block_content pos (grid, penguins) =
  match get_player_on pos penguins with
  | None -> Fish (Board.get_fish_nb pos grid)
  | Some player -> Player player

let block_content_to_string = function
  | Player p -> Player.to_string p
  | Fish f -> Board.fish_to_string f

let get_block_content_as_string pos gameState =
  block_content_to_string (get_block_content pos gameState)

let to_string (grid, penguins) =

  let colNb = Board.get_col_nb grid in
  let lineNb = Board.get_line_nb grid in

  let res = ref "" in

  for i = 0 to lineNb - 1 do
    if i mod 2 = 0 then begin
      for j = 0 to colNb - 1 do
        let content = get_block_content_as_string (j, i) (grid, penguins) in
        res := !res ^ "\\_/" ^ content;
      done;
      res := !res ^ "\\\n";
    end else begin
      for j = 0 to colNb -1 do
        let content = get_block_content_as_string (j, i) (grid, penguins) in
        res := !res ^ "/" ^ content ^ "\\_";
      done;
      res := !res ^ "/\n";
    end
  done;

  if lineNb mod 2 = 0 then begin
    for j = 0 to colNb - 1 do
      res := !res ^ "\\_/ ";
    done;
    res := !res ^ "\\\n";
  end else begin
    for j = 0 to colNb -1 do
      res := !res ^ "/ \\_";
    done;
    res := !res ^ "/\n";
  end;

  !res
