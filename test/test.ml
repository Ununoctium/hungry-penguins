(** Test file with tests, because tests are great *)

(** The tests **)

(** Basic test *)
let basic () =
  let expected = "jaimeleschips" in
  let got = "jaimeleschips" in
  Alcotest.(check string) "same string" got expected

(** Sets of test *)

(** Set of basic tests *)
let set_basic = [
  "Basic", `Quick, basic;
]

(** Main function, run all sets of tests *)
let () =
  Alcotest.run "Tests" [
    "Basic", set_basic;
  ]
